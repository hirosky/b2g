# B2G

## Running the Z' model

Copy the directory ZpAnomalonHZ_UFO to the madgraph/models directory

Start MG5_aMC in a shell with the ./bin/mg5_aMC  command,
use the following commands:

```
import model ZpAnomalonHZ_UFO
generate p p > ND ND~ , (ND > NS H , H > b b~) , (ND~ > NS~ Z , Z > mu+ mu-)
output
launch
```
* ND is the heavier neutral lepton (anomalon) and NS is the lighter one.
* Zp is the Z-prime boson.

In the  param_card   file you can change the following important parameters:
MND, MNS, MZp  are the masses   (default values: 300 GeV, 100 GeV, 1 TeV)
gZp  is the overall coupling of Zp to fermions  (default value: 0.4)


To decay Z to e+ e- replace   Z > mu+ mu-   by  Z > e+ e-

If you prefer to decay the Z and the Higgs with Pythia (this is not recommended usually because
Pythia does not keep track of spin correlations), then use:

generate p p > ND ND~ , ND > NS H , ND~ > NS~ Z

To find the total cross section it is safer to use
generate p p > ND ND~ 
and then multiply the cross section given by MG5_aMC by the branching fractions.
